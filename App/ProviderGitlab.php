<?php
namespace App;

class ProviderGitlab extends AbstractProvider
{

    protected $data = [
        "name" => "Gitlab",
        "redirect_uri" => "http://localhost:8000",
        "user-agent" => "provider"
    ];

    protected $clientId;
    protected $clientSecret;
    protected $uri = "https://https://gitlab.com/";
    protected $accessLink = "https://gitlab.com/oauth/authorize";
    protected $uriAuth = "https://gitlab.com/oauth/token";

    public function __construct(string $client_id, string $client_secret)
    {
        $this->provider = "Github";

        $this->clientId = $client_id;
        $this->clientSecret = $client_secret;
    }

    public function getUserData()
    {
        return $this->callback("/me");
    }
}