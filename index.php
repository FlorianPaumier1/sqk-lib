<?php
include("autoload.php");

use App\SdkFacade;


$sdk = new SdkFacade([
        [
            "name" => "Facebook",
            "client_id" => "",
            "client_secret" => ""
        ],
        [
            "name" => "Linkedin",
            "client_id" => "",
            "client_secret" => ""
        ],
        [
            "name" => "Github",
            "client_id" => "",
            "client_secret" => ""
        ],
        [
            "name" => "Cours",
            "client_id" => "",
            "client_secret" => ""
        ],
        [
            "name" => "Gitlab",
            "client_id" => "",
            "client_secret" => ""
        ]
    ]
);

if (!isset($_GET["code"])) {
    $links = $sdk->getLinks();
    foreach ($links as $key => $link){
        echo "<a href='".$link."'>".$key."</a><br><br><br>";
    }
} else {
    var_dump($sdk->getUserData());
}
